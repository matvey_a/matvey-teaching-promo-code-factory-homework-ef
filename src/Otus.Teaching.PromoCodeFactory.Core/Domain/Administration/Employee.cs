﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : BaseEntity
    {
        [Key]
        public Guid EmployeeId { get; set; } // TODO - обяъсни почему поменян ключ с обычного ID

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [MaxLength(256)]
        public string Email { get; set; }

        // TODO - разобраться со свзяью
        public Role Role { get; set; }

        [Required]
        public int AppliedPromocodesCount { get; set; }

        public override Guid GetId()
        {
            return EmployeeId;
        }
    }
}
﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
     public abstract class BaseEntity
    {
     //   public Guid Id { get; set; }
      public abstract Guid GetId();
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference : BaseEntity
    {
        [Key]
        public Guid PreferenceId { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public override Guid GetId()
        {
            return PreferenceId;
        }
    }
}
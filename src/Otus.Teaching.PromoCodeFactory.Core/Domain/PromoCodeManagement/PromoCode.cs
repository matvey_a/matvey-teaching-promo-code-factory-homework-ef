﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        [Key]
        public Guid PromoCodeId { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        [Required]
        public DateTime BeginDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [MaxLength(100)]
        public string PartnerName { get; set; }

        // TODO - make it work
        public Employee PartnerManager { get; set; }

        // TODO - make it work
        public Preference Preference { get; set; }

        // TODO - наладить связь с клиентом (1 - 1 / возможен null)

        public override Guid GetId()
        {
            return PromoCodeId;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        [Key]
        public Guid CustomerId { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }
        
        [MaxLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [MaxLength(256)]
        public string Email { get; set; }

        public override Guid GetId()
        {
            return CustomerId;
        }

        //TODO: Списки Preferences - (N - N)
        // и Promocodes 
    }
}